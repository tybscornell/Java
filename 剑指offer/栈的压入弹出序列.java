package 剑指offer;

/*
 * 题目描述
输入两个整数序列，第一个序列表示栈的压入顺序，请判断第二个序列是否可能为该栈的弹出顺序。假设压入栈的所有数字均不相等。例如序列1,2,3,4,5是某栈的压入顺序。
序列4,5,3,2,1是该压栈序列对应的一个弹出序列，但4,3,5,1,2就不可能是该压栈序列的弹出序列。（注意：这两个序列的长度是相等的）
 */
import java.util.*;
public class 栈的压入弹出序列 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int[] pushA = new int[n];
		int[] popA = new int[n];
		for(int i = 0; i < pushA.length; i++) pushA[i] = sc.nextInt();
		for(int i = 0; i < popA.length; i++) popA[i] = sc.nextInt();
		System.out.println(IsPopOrder(pushA, popA));
	}
	
	
	//判断栈的出栈顺序是否正确
	public static boolean IsPopOrder(int [] pushA,int [] popA) {
		//利用辅助栈进行测试判断
	    Stack<Integer> stack = new Stack<Integer>();
	    //出栈顺序下标从零开始
	    int pop_index = 0;
	    //将序列循环入栈
	    for(int i = 0; i < pushA.length; i++) {
	    	stack.push(pushA[i]);
	    	//若栈不为空且栈顶元素等于出栈元素
	    	while(!stack.empty() && stack.peek() == popA[pop_index]) {
	    		stack.pop();		//出栈
	    		pop_index++;		//出栈序列值累加
	    	}
	    }
	    //若最终栈不为空, 代表序列无法对应 ,返回false, 否则返回 true.
		return stack.isEmpty();
    }

}
