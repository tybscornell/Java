package 剑指offer;

/*
 * 题目描述
一只青蛙一次可以跳上1级台阶，也可以跳上2级……它也可以跳上n级。求该青蛙跳上一个n级的台阶总共有多少种跳法。
 */
import java.util.*;
public class 变态跳台阶 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		System.out.println(JumpFloorII(n));
	}
	

	//根据跳台阶的思想进行改进实现变态跳台阶的总次数
	public static int JumpFloorII(int target) {
		//定义初始化次数为 0
		int sum = 0;
		//若台阶数为 1或为 2则返回跳法 1或 2
		if(target == 1 || target == 2) return target;
		else {
			//近似于普通跳台阶的思想
			//总跳法为先跳1个台阶加先跳2个台阶...先跳n-1个台阶跳法之和加上1
			for(int i = 1; i < target; i++) {
				sum += JumpFloorII(target-i);
			}
		}
		//返回总的跳法
		return sum+1;
	}

}
