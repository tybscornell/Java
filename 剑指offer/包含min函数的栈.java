package 剑指offer;

import java.util.*;
public class 包含min函数的栈 {
	public static void main(String[] args) {
		
	}

}


class MyStack{
	//利用辅助栈存储最小值的信息
    Stack<Integer> stack = new Stack<Integer>();
    int[] array = new int[10000];
	int top = -1;
    public void push(int node) {
    	//自定义栈元素累加
        array[++top] = node;
        //当辅助栈为空时, 压入辅助栈
        if(stack.empty())
        	stack.push(node);
        //若此元素值小于最小值, 压入辅助栈
        else if(node <= stack.peek()) {
        	stack.push(node);
        }
    }
    
    //将栈顶元素移出, 若栈顶元素为最小值, 则将辅助栈栈顶弹出
    public void pop() {
    	int top1 = array[top];
        array[top--] = 0;
        //若弹出的值与最小值出辅助栈 
        if(top1 == stack.peek())
        	stack.pop();
    }
    
    //获取栈顶元素
    public int top() {
        return array[top];
    }
    
    //返回存储最小值的辅助栈顶元素, 即最小值
    public int min() {
        return stack.peek();
    }
	
}
