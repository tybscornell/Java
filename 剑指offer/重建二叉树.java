package 剑指offer;
/*
 * 题目描述
输入某二叉树的前序遍历和中序遍历的结果，请重建出该二叉树。
假设输入的前序遍历和中序遍历的结果中都不含重复的数字。
例如
前序遍历序列{1,2,4,7,3,5,6,8}
中序遍历序列{4,7,2,1,5,3,8,6}
重建二叉树并返回。
 */
import java.util.*;
public class 重建二叉树 {
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		TreeNode tree = null;
		int n = sc.nextInt();			//输入二叉树的结点个数
		int[] pre = new int[n];
		int[] mid = new int[n];
		for(int i = 0; i < n; i++) pre[i] = sc.nextInt();
		for(int i = 0; i < n; i++) mid[i] = sc.nextInt();
		tree = reConstructBinaryTree(pre,mid);
		printTree(tree, 1);
	}
	
	
	//根据先序序列和中序序列构建二叉树
	public static TreeNode reConstructBinaryTree(int [] pre,int [] in) {
		TreeNode tree = new TreeNode(pre[0]);		//创建该树
		
		//若数组的长度为 1 , 说明创建完毕, 说明此树已无左右子树
		if(pre.length == 1 && in.length ==1){
			tree.left = null;
			tree.right = null;
			return tree;
		}
		
		//检索先序序列的第一个根子树在中序序列的位置
		int first = indexValue(in, pre[0]);
		
		//若在中序的位置为 0, 说明此树无左子树
		if(first == 0){
			int[] new_pre = getArray(pre,1,pre.length);
			int[] new_in = getArray(in,1,in.length);		//创建子序列
			
			//递归调用创建左右子树
			tree.left = null;
			tree.right = reConstructBinaryTree(new_pre,new_in);

		//若在中序的位置为 最后一个位置length-1, 说明此树无右子树
		}else if(first == in.length-1){
			int[] new_pre = getArray(pre,1,pre.length);
			int[] new_in = getArray(in,0,in.length-1);		//创建子序列
			
			//递归调用创建左右子树
			tree.left = reConstructBinaryTree(new_pre,new_in);
			tree.right = null;
			
		//若在中序的中间任一位置
		}else{
			int[] pre_left = getArray(pre,1,first+1);
			int[] in_left = getArray(in, 0, first);					//创建此树的左子树序列
			
			int[] pre_right = getArray(pre,first+1,pre.length);
			int[] in_right = getArray(in, first+1, in.length);		//创建此树的右子树序列
			
			//递归调用创建左右子树
			tree.left = reConstructBinaryTree(pre_left,in_left);
			tree.right = reConstructBinaryTree(pre_right,in_right);
		}
		
        return tree;	//返回树根
    }
	
	//检索一个数在整型数组中的值
	public static int indexValue(int[] array, int value){
		for(int i = 0; i < array.length; i++){
			if(array[i] == value) return i;
		}
		return -1;
	}
	
	//返回一个数组中的下标从i个到j-1的子数组
	public static int[] getArray(int[] array, int i, int j){
		int[] new_array = new int[j-i];
		for(int k = i; k < j; k++){
			new_array[k-i] = array[k];
		}
		return new_array;
	}
	
	//打印数组中的所有数据
	public static void printArray(int[] array){
		for(int i = 0; i < array.length; i++) System.out.print(array[i] + " ");
		System.out.println();
	}
	
	//以逆时针旋转 90度打印二叉树
	public static void printTree(TreeNode tree, int h){
		if(tree == null) return;
		printTree(tree.right, h+1);
		for(int i = 0; i < h; i++){
			System.out.print("    ");
		}
		System.out.println(tree.val);
		printTree(tree.left, h+1);
	}

}

//二叉树结点
class TreeNode{
	int val;
	TreeNode left = null;
	TreeNode right = null;
	public TreeNode(int val){
		this.val = val;
	}
}
