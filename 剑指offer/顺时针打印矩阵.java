package 剑指offer;

/*
 * 题目描述
输入一个矩阵，按照从外向里以顺时针的顺序依次打印出每一个数字。
例如，如果输入如下4 X 4矩阵： 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 则依次打印出数字1,2,3,4,8,12,16,15,14,13,9,5,6,7,11,10.
 */
import java.util.*;
public class 顺时针打印矩阵 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		ArrayList<Integer> array = null;
		int n = sc.nextInt();
		int m = sc.nextInt();
		int[][] mat = new int[n][m];
		for(int i = 0; i < n; i++) {
			for(int j = 0; j < m; j++) {
				mat[i][j] = sc.nextInt();
			}
		}
		array = printMatrix(mat);
		System.out.println(array);
	}
	
	
	//顺时针输出矩阵
	public static ArrayList<Integer> printMatrix(int [][] matrix) {
	       //定义存放整型数据的集合
		   ArrayList<Integer> array = new ArrayList<Integer>();
	       //定义抽象扫描光标,从位置0,0开始扫描添加
		   int start_x = 0;
	       int start_y = 0;
	       //定义每一行和每一列的结束界限
	       int end_x = matrix[0].length-1;
	       int end_y = matrix.length-1;
	       //进行顺时针扫描矩阵, 当start_x == end_x或start_y == end_y时结束扫描
	       while(start_x < end_x && start_y < end_y) {
	    	   //定义移动光标, 循环一次等于扫描矩阵最外层的一圈
	    	   int x = start_x;
	    	   int y = start_y;
	    	   //扫描除了右顶点的最上层
	    	   while(x < end_x) {
	    		   array.add(matrix[y][x]);
	    		   x++;
	    	   }
	    	   //扫描除了下顶点的最右层
	    	   while(y < end_y) {
	    		   array.add(matrix[y][x]);
	    		   y++;
	    	   }
	    	   //扫描除了左顶点的最下层
	    	   while(x > start_x) {
	    		   array.add(matrix[y][x]);
	    		   x--;
	    	   }
	    	   //扫描除了上顶点的最左层
	    	   while(y > start_y) {
	    		   array.add(matrix[y][x]);
	    		   y--;
	    	   }
	    	   //将矩阵最层的数据已全部添加到集合中
	    	   //开始光标累加
	    	   start_x++;
	    	   start_y++;
	    	   //结束点累减
	    	   end_x--;
	    	   end_y--;
	    	   //开始下一层的矩阵扫描
	       }
	       //若最终仅剩下一列未扫描完, 将这一列全部添加至集合
	       if(start_x == end_x) {
	    	   for(int i = start_y; i <= end_y; i++)
	    		   array.add(matrix[i][start_x]);
	       //若最终仅剩下一行未扫描完, 将这一行全部添加至集合
	       }else if(start_y == end_y) {
	    	   for(int i = start_x; i <= end_x; i++)
	    		   array.add(matrix[start_y][i]);
	       }
	       //返回顺时针扫描后的矩阵集合
	       return array;
    }

}
