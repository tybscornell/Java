package 剑指offer;

/*
 * 题目描述
输入一个整数数组，实现一个函数来调整该数组中数字的顺序，使得所有的奇数位于数组的前半部分。
所有的偶数位于数组的后半部分，并保证奇数和奇数，偶数和偶数之间的相对位置不变。
 */
import java.util.*;
public class 调整数组顺序使奇数位于偶数前面 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int[] arr = new int[6];
		for(int i = 0; i < 6; i++) {
			arr[i] = i+1;
		}
		reOrderArray(arr);
	}

	//按照规定排列数组
	public static void reOrderArray(int [] array) {
        //置数组首奇数应插入在数组中的首位置start_x为0
		//初始化遍历访问数组的计数器array_x为0
		int start_x = 0, array_x = 0;
		//通过循环对数组中的每个数进行遍历访问操作
        while(array_x < array.length) {
        	//若此数为奇数, 将此数插入到 start_x位置
        	if(array[array_x]%2 != 0) {
        		int temp = array[array_x];	//记录该数
        		//对start_x位置到array_x位置上的数进行后挪
        		for(int j = array_x; j > start_x; j--) {
        			array[j] = array[j-1];
        		}
        		//将此奇数进行插入
        		array[start_x] = temp;
        		//下次奇数插入位置累加
        		start_x++;
        	}
        	//遍历数组下标累加
        	array_x++;
        }
    }
}
