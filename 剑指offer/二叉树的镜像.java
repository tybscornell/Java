package 剑指offer;

/*
 * 题目描述
操作给定的二叉树，将其变换为源二叉树的镜像。
输入描述:
二叉树的镜像定义：源二叉树 
    	    8
    	   /  \
    	  6   10
    	 / \  / \
    	5  7 9 11
    	镜像二叉树
    	    8
    	   /  \
    	  10   6
    	 / \  / \
    	11 9 7  5
 */
import java.util.*;
public class 二叉树的镜像 {
	public static void main(String[] args) {
		
	}
	/**
	public class TreeNode {
	    int val = 0;
	    TreeNode left = null;
	    TreeNode right = null;

	    public TreeNode(int val) {
	        this.val = val;

	    }

	}
	*/
	//将给定二叉树转化为源二叉树的镜像
	public static void Mirror(TreeNode root) {
        if(root == null) return;
        //将二叉树的左右树进行交换
        TreeNode temp = root.left;
        root.left = root.right;
        root.right = temp;
        //再进行子树的交换
        Mirror(root.left);
        Mirror(root.right);
        return;
    }

}
