package 剑指offer;


/*
 * 题目描述
给定一个double类型的浮点数base和int类型的整数exponent。求base的exponent次方。

保证base和exponent不同时为0
 */
import java.util.*;
public class 数值的整次数方 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double base = sc.nextDouble();
		int exponent = sc.nextInt();
		System.out.println(Power(base, exponent));
	}
	
	public static double Power(double base, int exponent) {
        double sum = 1.0;
        int max = Math.abs(exponent);
        if(exponent == 0) return sum;
        while(max != 0) {
        	if((max & 1) == 1)		//若二进制max在此位为1,则进行累乘
        		sum *= base;		//sum累乘
        	base *= base;			//base累乘
        	max >>= 1;				//max向右进行移位
        }
        if(exponent > 0) return sum;
        else return 1/sum;
    }

}
