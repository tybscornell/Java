package 剑指offer;

import java.util.*;
public class 斐波那契数列 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("请输入一个数  n ：");
		int n = sc.nextInt();
		System.out.println("这个数在斐波那契数列中的值为：\n" + getNum(n));
	}
	public static int getNum(int n) {
		if(n == 0) return 0;
		if(n == 1) return 1;
		int p = 0, q = 1, sum = 0;
		for(int i = 2; i <= n; i++) {
			sum = p + q;		//产生新的数据
			p = q;		//前边的一个数后挪
			q = sum;		//后边的一个数后挪
		}
		return sum;
	}
}
