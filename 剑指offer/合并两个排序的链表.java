package 剑指offer;

/*
 * 题目描述
输入两个单调递增的链表，输出两个链表合成后的链表，当然我们需要合成后的链表满足单调不减规则。
 */
import java.util.*;
public class 合并两个排序的链表 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		ListNode[] node1 = null;
		ListNode[] node2 = null;
		ListNode union = null;
		int n, m;
		n = sc.nextInt();
		m = sc.nextInt();
		node1 = new ListNode[n];
		node2 = new ListNode[m];
		for(int i = 0; i < n; i++) node1[i] = new ListNode(2*i + 1);
		for(int i = 0; i < m; i++) node2[i] = new ListNode(2*i);
		for(int i = 0; i < n-1; i++) node1[i].next = node1[i+1];
		node1[n-1].next = null;
		for(int i = 0; i < m-1; i++) node2[i].next = node2[i+1];
		node2[m-1].next = null;
		union = Merge(node1[0], node2[0]);
		while(union != null) {
			System.out.print(union.val + " ");
			union = union.next;
		}
	}
	
	
	public static ListNode Merge(ListNode list1,ListNode list2) {
        ListNode head = null, pre = null;
        if(list1 == null) return list2;
        if(list2 == null) return list1;
        //以两个头结点中较小的结点作为 联合链表的头结点
        //若list1.val <= list2.val
        if(list1.val <= list2.val) {
        	head = list1;			//头结点为 list1头结点
        	pre = head;				//pre始终指向新链表的最新结点
        	list1 = list1.next;		//链表list1的头结点进行后挪
        }else {
        	head = list2;			//头结点为 list2头结点
        	pre = head;				//pre始终指向新链表的最新结点
        	list2 = list2.next;		//链表list2的头结点进行后挪
        }
        //若链表均不结束, 则一直进行比较连接
        while(list1 != null && list2 != null) {
        	//同上, 若list1.val <= list2.val
        	if(list1.val <= list2.val) {
        		pre.next = list1;		//新链表的下个结点指向list1头结点
        		pre = list1;			//新链表的下个结点后挪
        		list1 = list1.next;		//list1头结点后挪
        	}else {
        	//同上
        		pre.next = list2;
        		pre = list2;
        		list2 = list2.next;
        	}
        }
        //若链表list1先结束, 则将链表 list2的结点全部连接到新链表中，反之亦然
        if(list1 == null) pre.next = list2;
        else pre.next = list1;
        return head;
    }

}
