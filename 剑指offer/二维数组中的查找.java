package 剑指offer;

/*
 * 题目描述
在一个二维数组中（每个一维数组的长度相同），每一行都按照从左到右递增的顺序排序，每一列都按照从上到下递增的顺序排序。
请完成一个函数，输入这样的一个二维数组和一个整数，判断数组中是否含有该整数。
 */
import java.util.*;
public class 二维数组中的查找 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int m = sc.nextInt();
		int[][] mat = new int[n][m];
		for(int i = 0; i < n; i++) {
			for(int j = 0; j < m; j++) {
				mat[i][j] = sc.nextInt();
			}
		}
		System.out.println(Find(7, mat));
	}
	
	/*
	由于矩阵排列规律为:  从左到右递升, 从上到下递升
	可分别从左下角或右上角进行检索target
	左上角:     若 target == array[start_x][start_y], 返回true
	                     若 target >  array[start_x][start_y], start_y--;
	                     若 target <  array[start_x][start_y], start_x++; 
	           
	           右上角检索同理, 以下为从右下角进行检索的程序
	 */
	public static boolean Find(int target, int [][] array) {
		int start_x = array[0].length-1, start_y = 0;
		while(start_x >= 0 && start_y < array.length) {
			if(array[start_y][start_x] == target) return true;
			else if(array[start_y][start_x] > target) start_x--;
			else start_y++;
		}
		return false;
    }

}
