package 剑指offer;
/*
 * 题目描述
用两个栈来实现一个队列，完成队列的Push和Pop操作。 队列中的元素为int类型。
 */
import java.util.*;
public class 用两个栈实现队列 {
	static Stack<Integer> stack1 = new Stack<Integer>();
    static Stack<Integer> stack2 = new Stack<Integer>();
	public static void main(String[] args){
		for(int i = 0; i < 5; i++) {
			push(i+1);
		}
		for(int i = 0; i < 4; i++) {
			System.out.println(pop());
		}
		for(int i = 5; i < 10; i++) {
			push(i+1);
		}
		for(int i = 4; i < 10; i++) {
			System.out.println(pop());
		}
	}
    
	//栈stack1模拟队列Queue只进行入栈即入队操作
    public static void push(int node) {
    	stack1.push(node);
    }
    
    //模拟队列Queue出队操作, 即让队头结点出队, 借助入栈stack2则队头元素在栈顶
    //移出栈stack2的栈顶元素,再全部将数据入栈stack1
    public static int pop() {
    	int node;
    	
    	//栈stack1元素全部出栈进入栈stack2,此时队头元素在栈stack2栈顶
    	while(!stack1.empty()) {
    		int temp;
    		temp = stack1.pop();
    		stack2.push(temp);
    	}
    	
    	//将栈顶元素即队头元素移出
    	node = stack2.pop();
    	
    	//再次将所有元素返回栈stack1;
    	while(!stack2.empty()) {
    		int temp;
    		temp = stack2.pop();
    		stack1.push(temp);
    	}
    	//返回队头元素
    	return node;
    }

}
