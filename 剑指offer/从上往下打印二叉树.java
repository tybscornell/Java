package 剑指offer;


/*
 * 题目描述
从上往下打印出二叉树的每个节点，同层节点从左至右打印。
public class TreeNode {
    int val = 0;
    TreeNode left = null;
    TreeNode right = null;

    public TreeNode(int val) {
        this.val = val;

    }

}
*/
import java.util.*;
public class 从上往下打印二叉树 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		ArrayList<Integer> array = new ArrayList<Integer>(); 
		int n = sc.nextInt();
		int[] pre = new int[n];
		int[] mid = new int[n];
		for(int i = 0; i < pre.length; i++) pre[i] = sc.nextInt();
		for(int i = 0; i < mid.length; i++) mid[i] = sc.nextInt();
		重建二叉树 tree = new 重建二叉树();
		TreeNode root = null;
		root = tree.reConstructBinaryTree(pre, mid);
		array = PrintFromTopToBottom(root);
		System.out.println(array);
	}
	
	
	//利用队列的思想从上到小, 从左至右遍历二叉树
	public static ArrayList<Integer> PrintFromTopToBottom(TreeNode root) {
		//定义存储树信息队列
        LinkedList<TreeNode> queue = new LinkedList<TreeNode>();
        //定义存储顶点信息的集合
        ArrayList<Integer> array = new ArrayList<Integer>();
        if(root == null) return array;
        //定义临时树根,方便队列对二叉树的进队出队操作
        TreeNode all_root = null;
        //将树根进入队列
        queue.add(root);
        //假如队列不为空
        while(!queue.isEmpty()) {
        	//出队
        	all_root = queue.poll();
        	//将数据存放到集合中
        	array.add(all_root.val);
        	//若左子树不为空, 添加左子树至队列
        	if(all_root.left != null)
        		queue.add(all_root.left);
        	//若右子树不为空, 添加右子树至队列
        	if(all_root.right != null)
        		queue.add(all_root.right);
        }
        //返回存放树根信息的集合
        return array;
		
    }

}
