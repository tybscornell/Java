package 剑指offer;


/*
 * 题目描述
输入两棵二叉树A，B，判断B是不是A的子结构。（ps：我们约定空树不是任意一个树的子结构）
 */
import java.util.*;
public class 树的子结构 {
	public static void main(String[] args) {
		
	}
	
	
	//判断树root2是否为root1的子树
	public static boolean HasSubtree(TreeNode root1,TreeNode root2) {
		if(root2 == null) return false;
	    String s1 = "";
	    String s2 = "";
	    s1 = getSequence(root1, s1);		//获取树root1的先序访问序列
	    s2 = getSequence(root2, s2);		//获取树root2的先序访问序列
	    //如果树root1的访问序列中包含root2的访问序列, 则为子树, 否则不是
	    if(s1.indexOf(s2) != -1) return true;
	    else return false;
	}
				
				
	//将一个二叉树的所有根节点数据利用先序遍历二叉树连接到一个字符串中
	public static String getSequence(TreeNode root, String sequence) {
		if(root == null) return sequence;
		else {
			sequence += Integer.toString(root.val);			//添加根节点数据
			sequence = getSequence(root.left, sequence);	//访问左子树
			sequence = getSequence(root.right, sequence);	//访问右子树
		}
		//返回这个字符串
		return sequence;
	}

}
