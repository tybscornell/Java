package 剑指offer;

/*
 * 题目描述
我们可以用2*1的小矩形横着或者竖着去覆盖更大的矩形。请问用n个2*1的小矩形无重叠地覆盖一个2*n的大矩形，总共有多少种方法？
 */
import java.util.*;
public class 矩形覆盖 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		System.out.println(RectCover(n));
	}
	
	
	//矩形覆盖问题, 斐波那契数列
	public static int RectCover(int target) {
		//若 n==1, 仅有一种方法覆盖
		if(target <= 1) return 1;
		//若 n==2, 则有两种方法覆盖
		else if(target == 2) return target;
		else {
			//否则覆盖方法为 首先覆盖一个 和首先覆盖两个大矩形的和
			return RectCover(target-1) + RectCover(target-2);
		}
		
    }

}
