package 剑指offer;

/*
 * 题目描述
把一个数组最开始的若干个元素搬到数组的末尾，我们称之为数组的旋转。
输入一个非递减排序的数组的一个旋转，输出旋转数组的最小元素。
例如数组{3,4,5,1,2}为{1,2,3,4,5}的一个旋转，该数组的最小值为1。
NOTE：给出的所有元素都大于0，若数组大小为0，请返回0。
 */
import java.util.*;
public class 旋转数组的最小数字 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int[] array = new int[20];
		for(int i = 0; i < 20; i++) {
			array[i] = sc.nextInt();
		}
		System.out.println(getMin(array));
	}
	
	//采用二分法查找递增有序的旋转数组中的最小值
	public static int getMin(int[] array) {
		int left = 0;
		int right = array.length-1;
		int mid;
		while(left+1 < right) {
			mid = (left + right)/2;
			
			//判断最小值所在的位置并移动指针
			if(array[mid] > array[0]) {
				
				//若array[mid] > array[0], 说明此时 array[mid]在前边递增子数组
				//则最小值在mid右边, 改变左指针 left的位置指向中间, 缩小查找范围
				left = mid;
			}else if(array[mid] < array[0]){
				
				//若array[mid] < array[0], 说明此时 array[mid]在后边递增子数组
				//则最小值在mid左边, 改变右指针 right的位置指向中间, 缩小查找范围
				right = mid;
			}else {
				
				//若array[mid] == array[0], 调用函数判断mid所属前后子数组
				if(isPreArray(array, mid)) left = mid;
				else right = mid;
			}
		}
		return array[right];
	}
	
	public static boolean isPreArray(int[] array, int val) {
		int i = 1;
		while(array[val] == array[val-i]) {
			i++;
			if(val-i == -1) return true;	//说明 val值在前边升序子数组
		}
		return false;					//说明val值在后边升序子数组
	}

}
