package 剑指offer;

/*
 * 题目描述
输入一个链表，反转链表后，输出新链表的表头。
 */
import java.util.*;
public class 反转链表 {
	//测试方法
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		ListNode[] node = new ListNode[n];
		ListNode temp;
		for(int i = 0; i < n; i++) {
			node[i] = new ListNode(i+1);
		}
		for(int i = 0; i < n-1; i++) {
			node[i].next = node[i+1];
		}
		node[n-1].next = null;
		temp = node[0];
		while(temp != null) {
			System.out.print(temp.val + " ");
			temp = temp.next;
		}
		System.out.println();
		temp = ReverseList(node[0]);
		while(temp != null) {
			System.out.print(temp.val + " ");
			temp = temp.next;
		}
	}
	
	
	//高效率实现反转链表
	public static ListNode ReverseList(ListNode head) {
		//若表头为空, 返回空表头head;
		if(head == null) return head;
		//定义指向链表前驱和后继的指针 pre 和 next
		ListNode pre = null;
		ListNode next = null;
		//反转链表的具体循环操作
		//利用临时变量 next 暂时存放 head 的后继
		//使head的next指针指向其前驱变量, 首次循环指向为空
		//将head赋值给pre即 前驱变量 pre后移
		//将next赋值给head 即后继变量 head后移
		while(head != null) {
			next = head.next;
			head.next = pre;	//原后继结点指向其前驱
			pre = head;			//前驱结点后挪
			head = next;		//后继结点后挪
		}
		//当 head 为空时, 此时的pre即为新链表的头结点
		return pre;
	}
	

	//反转链表
//	public static ListNode ReverseList(ListNode head) {
//		//原链表为空返回空
//		if(head == null) return null;
//		//记录链表结点个数sum
//		int sum = 0;
//		//定义临时结点存放原头结点
//		ListNode temp = head;
//		//定义结点数组
//		ListNode[] arr = null;
//		//统计结点个数
//		while(temp != null) {
//			sum++;
//			temp = temp.next;
//		}
//		//生成长度为链表结点个数的结点数组
//		arr = new ListNode[sum];
//		//temp重新指向原链表头
//		temp = head;
//		//将原链表结点依次存放到结点数组中
//		for(int i  = 0; i < sum; i++) {
//			arr[i] = temp;
//			temp = temp.next;
//		}
//		//结点数组从最后结点出发作为表头依次连接其它结点
//		for(int i = sum-1; i > 0; i--) {
//			arr[i].next = arr[i-1];
//		}
//		//最后结点的指向为 null
//		arr[0].next = null;
//		//返回表头结点
//		return arr[sum-1];
//    }

}
