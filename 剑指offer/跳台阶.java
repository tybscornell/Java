package 剑指offer;

/*
 * 题目描述(递归问题)
一只青蛙一次可以跳上1级台阶，也可以跳上2级。求该青蛙跳上一个n级的台阶总共有多少种跳法（先后次序不同算不同的结果）。
 */
import java.util.*;
public class 跳台阶 {
	public static int sum = 0;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		System.out.println(JumpFloor(n));
	}
	
	//从下往上进行跳台阶, 若台阶数大于等于3, 将第一次跳1个台阶的跳法和第一次跳2个台阶的跳法相加,即为总的跳法
	public static int JumpFloor(int target) {
		//当台阶数剩下 1 个台阶共有一种跳法, 返回1;台阶数剩下 2 个台阶时有两种跳法,返回2
		if(target == 1 || target == 2) return target;
		//跳的全部次数为 第一次跳 1 个台阶 加上 第一次跳 2 个台阶
		else return JumpFloor(target-1) + JumpFloor(target-2);
		
    }

}
