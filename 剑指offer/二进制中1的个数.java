package 剑指offer;

/*
 * 题目描述
输入一个整数，输出该数二进制表示中1的个数。其中负数用补码表示。
 */
import java.util.*;
public class 二进制中1的个数 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		System.out.println(NumberOf1(n));
	}
	
	public static int NumberOf1(int n) {
		int sum = 0, i = 0, temp = 1;
		//判断这个数的每一位二进制数是否为 1
		while(i < 32) {
			//temp 为 1,2,4,8...
			//若 n & temp == temp 说明 n的第 i 位数为1
			if((n & temp) == temp)
				sum++;			//计数器累加
			i++;				//n的二进制位数累加
			temp <<= 1;			//temp左移即累乘 2
		}
		//返回次数
		return sum;
    }
}
