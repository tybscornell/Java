package 剑指offer;


/*
 * 题目描述
写一个函数，求两个整数之和，要求在函数体内不得使用+、-、*、/四则运算符号。
 */
import  java.util.*;
public class 不用加减乘除做加法 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int a = sc.nextInt();
		int b = sc.nextInt();
		System.out.println(Add(a, b));
	}
	
	
	//不通过加减乘除运算符得到两个数的和
	public static int Add(int num1,int num2) {
        while(num2 != 0) {
        	int temp = num1^num2;			//通过两个数的异或操作,得到未进位时的相加数字
        	num2 = (num1&num2) << 1;		//通过两个数的与操作, 得到进位的数字,若进位的数字为0, 循环结束
        	num1 = temp;					//将此数赋值给 num1
        }
        return num1;
    }

}
