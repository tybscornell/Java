package 剑指offer;
/*
	题目描述
	输入一个链表，按链表从尾到头的顺序返回一个ArrayList。
 */
import java.util.*;
public class 从尾到头打印链表 {
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		ListNode[] list_node = new ListNode[10];
		ArrayList<Integer> array;
		for(int i = 0; i < list_node.length; i++){
			list_node[i] = new ListNode(i+1);
			if(i != 0) list_node[i-1].next = list_node[i];
		}
		array = printListFromTailToHead(list_node[0]);
		System.out.println(array);
	}
	
	//将输入的链表从尾到头返回一个ArrayList
	public static ArrayList<Integer> printListFromTailToHead(ListNode listNode) {
        ArrayList<Integer> array = new ArrayList();
        Stack<ListNode> stack = new Stack();
        
        //将链表中的结点listNode全部入栈stack
        while(listNode != null){
        	stack.push(listNode);
        	listNode = listNode.next;
        }
        
        //将栈内的元素全部输出添加到类集array中
        while(!stack.empty()){
        	array.add(stack.pop().val);
        }
        
        return array;
    }

}

//Java中的链表类
class ListNode{
	int val;
	ListNode next = null;
	public ListNode(int val){
		this.val = val;
	}
}