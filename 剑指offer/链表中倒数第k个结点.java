package 剑指offer;

/*
 * 题目描述
输入一个链表，输出该链表中倒数第k个结点。
 */
import java.util.*;
public class 链表中倒数第k个结点 {
	//测试方法
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int x = sc.nextInt();
		ListNode[] node = new ListNode[n];
		ListNode temp;
		for(int i = 0; i < n; i++) {
			node[i] = new ListNode(i+1);
		}
		for(int i = 0; i < n-1; i++) {
			node[i].next = node[i+1];
		}
		node[n-1].next = null;
		temp = node[0];
		while(temp != null) {
			System.out.print(temp.val + " ");
			temp = temp.next;
		}
		System.out.println("\n" + FindKthToTail(node[0], x).val);
	}
	
	//求链表中倒数第 k 个结点
	public static ListNode FindKthToTail(ListNode head,int k) {
		int sum = 0;
		ListNode temp = head;
		//统计链表结点的总个数
		while(temp != null) {
			sum++;
			temp = temp.next;
		}
		//如果倒数出界,返回空
		if(k > sum) return null;
		//temp 再次指向头结点
		temp = head;
		//得到倒数第k个结点在链表中的位置
		for(int i = 1; i < sum-k+1; i++) {
			temp = temp.next;
		}
		//返回此结点
		return temp;
    }

}
