package 蓝桥杯;

/*
 * 问题描述
　　在数列 a[1], a[2], ..., a[n] 中，如果 a[i] < a[i+1] < a[i+2] < ... < a[j]，则称 a[i] 至 a[j] 为一段递增序列，长度为 j-i+1。
　　给定一个数列，请问数列中最长的递增序列有多长。
输入格式
　　输入的第一行包含一个整数 n。
　　第二行包含 n 个整数 a[1], a[2], ..., a[n]，相邻的整数间用空格分隔，表示给定的数列。
输出格式
　　输出一行包含一个整数，表示答案。
样例输入
7
5 2 4 1 3 7 2
样例输出
3
 */
import java.util.*;
public class 递增数列 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n;
		int[] temp;
		n = sc.nextInt();
		temp = new int[n];
		for(int i = 0; i < temp.length; i++) temp[i] = sc.nextInt();
		System.out.println(getMax(temp));
		
	}
	public static int getMax(int[] temp) {
		int max = 0, start = 0;
		while(start < temp.length-1) {
			int max_o = 0;
			while(temp[start] < temp[start+1] && start < temp.length -1) {
				start++;
				max_o++;
			}
			if(max_o > max) max = max_o;
			if(start < temp.length-1) start++;
		}
		return max+1;
	}

}
