package 蓝桥杯;

/*
 * 问题描述
　　一般情况下，如果一个单词在段首，则第一个字母大写，后面的字母小写。
　　给定一个单词，单词中可能包含大小写字母，请按第一个字母大写，后面字母小写的方式输出。
输入格式
　　输入一行，包含一个单词，单词中只包含大写或小写英文字母。
输出格式
　　输出单词在段首时的形式，第一个字母大写，其他字母小写。
样例输入
LanQiao
样例输出
Lanqiao
样例输入
cUp
样例输出
Cup
 */
import java.util.*;
public class 格式化单词 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String word = sc.nextLine();
		System.out.println(getWord(word));
	}
	public static String getWord(String str) {
		if(str == null) return null;
		char[] word = new char[str.length()];
		if(str.charAt(0) >= 'a' && str.charAt(0) <= 'z')
			word[0] = (char)(str.charAt(0)-32);
		else
			word[0] = str.charAt(0);
		for(int i = 1; i < str.length(); i++) {
			if(str.charAt(i) >= 'A' && str.charAt(i) <= 'Z')
				word[i] = (char)(str.charAt(i)+32);
			else
				word[i] = str.charAt(i);
		}
		return new String(word);
	}

}
