package 蓝桥杯;

/*
 * 问题描述
　　一个平面向量表示从一个坐标点到另一个坐标点的变化量，一般用两个数 (x, y) 来表示。
　　两个向量相加是指分别将对应的两个数相加，例如 (x_1, y_1) 与 (x_2, y_2) 相加后得 (x_1+x_2, y_1+y_2)。
　　如果两个向量相加后，得到的向量两个值相等，我们称这两个向量为和谐向量对。例如 (3, 5) 和 (4, 2) 是和谐向量对。
　　给定 n 个向量，问能找到多少个和谐向量对？
输入格式
　　输入的第一行包含一个整数 n，表示向量的个数。
　　接下来 n 行，每行两个整数 x_i, y_i，表示一个向量。
输出格式
　　输出一行，包含一个整数，表示有多少个和谐向量对。
　　请注意，自己和自己不能成为一个和谐向量对。
样例输入
5
9 10
1 3
5 5
5 4
8 6
样例输出
2
样例输入
4
1 1
2 2
1 1
2 2
样例输出
6
样例说明
　　每两个向量组成一个和谐向量对。
评测用例规模与约定
　　对于 70% 的评测用例，1 <= n <= 1000；
　　对于所有评测用例，1 <= n <= 100000，-1000000 <= x_i, y_i <= 1000000。
　　请注意答案可能很大，可能需要使用 long long 来保存。
 */
import java.util.*;
public class 统计和谐向量 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n;
		int[][] temp;
		n = sc.nextInt();
		temp = new int[n][2];
		for(int i = 0; i < n; i++) {
			temp[i][0] = sc.nextInt();
			temp[i][1] = sc.nextInt();
		}
		System.out.println(getSum(temp));
	}
	public static long getSum(int[][] temp) {
		long sum = 0;
		for(int i = 0; i < temp.length-1; i++) {
			for(int j = i+1; j < temp.length; j++) {
				if(temp[i][0]+temp[j][0] == temp[i][1]+temp[j][1])
					sum++;
			}
		}
		return sum;
	}

}
