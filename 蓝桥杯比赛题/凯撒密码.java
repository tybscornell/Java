package 蓝桥杯;

/*
 * 问题描述
　　给定一个单词，请使用凯撒密码将这个单词加密。
　　凯撒密码是一种替换加密的技术，单词中的所有字母都在字母表上向后偏移3位后被替换成密文。即a变为d，b变为e，...，w变为z，x变为a，y变为b，z变为c。
　　例如，lanqiao会变成odqtldr。
输入格式
　　输入一行，包含一个单词，单词中只包含小写英文字母。
输出格式
　　输出一行，表示加密后的密文。
样例输入
lanqiao
样例输出
odqtldr
 */
import java.util.*;
public class 凯撒密码 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String txt = sc.nextLine();
		System.out.println(getPass(txt));
	}
	public static String getPass(String txt) {
		char[] pass = new char[txt.length()];
		for(int i = 0; i < txt.length(); i++) {
			if(txt.charAt(i) == 'x') pass[i] = 'a';
			else if(txt.charAt(i) == 'y') pass[i] = 'b';
			else if(txt.charAt(i) == 'z') pass[i] = 'c';
			else pass[i] = (char)(txt.charAt(i)+3);
		}
		return new String(pass);
	}

}
