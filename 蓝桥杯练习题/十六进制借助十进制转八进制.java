package 蓝桥杯练习题;

/*
 * 问题描述
　　给定n个十六进制正整数，输出它们对应的八进制数。

输入格式
　　输入的第一行为一个正整数n （1<=n<=10）。
　　接下来n行，每行一个由0~9、大写字母A~F组成的字符串，表示要转换的十六进制正整数，每个十六进制数长度不超过100000。

输出格式
　　输出n行，每行为输入对应的八进制正整数。

　　【注意】
　　输入的十六进制数不会有前导0，比如012A。
　　输出的八进制数也不能有前导0。

样例输入
　　2
　　39
　　123ABC

样例输出
　　71
　　4435274

　　【提示】
　　先将十六进制数转换成某进制数，再由某进制数转换成八进制。
 */
import java.util.*;
public class 十六进制借助十进制转八进制 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		while(n-- > 0) {
			long num = 0;
			String hex = sc.next();
			num = getDecimal(hex);
			System.out.println(getOctal(num));
		}
	}
	//十六进制数转化为八进制数
	public static long getDecimal(String hex) {
		long num = 0;
		for(int i = hex.length()-1; i >= 0 ; i--) {
			char c = hex.charAt(hex.length()-1-i);
			if(c >= '0' && c <= '9') {
				num += (int)(c - '0') * Math.pow(16, i);
			}
			else {
				switch(c) {
				case 'A':
				case 'a':
					num += 10 * Math.pow(16, i);
					break;
				case 'B':
				case 'b':
					num += 11 * Math.pow(16, i);
					break;
				case 'C':
				case 'c':
					num += 12 * Math.pow(16, i);
					break;
				case 'D':
				case 'd':
					num += 13 * Math.pow(16, i);
					break;
				case 'E':
				case 'e':
					num += 14 * Math.pow(16, i);
					break;
				case 'F':
				case 'f':
					num += 15 * Math.pow(16, i);
					break;
				}
			}
		}
		return num;
	}
	//将十进制数转化为八进制数
	public static long getOctal(long num) {
		if(num == 0) return num;
		else return num%8 + 10 * getOctal(num/8);
	}

}
