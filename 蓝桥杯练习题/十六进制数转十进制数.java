package 蓝桥杯练习题;

/*
 * 问题描述
　　从键盘输入一个不超过8位的正的十六进制数字符串，将它转换为正的十进制数后输出。
　　注：十六进制数中的10~15分别用大写的英文字母A、B、C、D、E、F表示。
样例输入
	FFFF
样例输出
	65535
 */
import java.util.*;
public class 十六进制数转十进制数 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String hex = sc.next();
		System.out.println(getDecimal(hex));
	}
	public static long getDecimal(String hex) {
		long num = 0;
		for(int i = hex.length()-1; i >= 0 ; i--) {
			char c = hex.charAt(hex.length()-1-i);
			if(c >= '0' && c <= '9') {
				num += (int)(c - '0') * Math.pow(16, i);
			}
			else {
				switch(c) {
				case 'A':
				case 'a':
					num += 10 * Math.pow(16, i);
					break;
				case 'B':
				case 'b':
					num += 11 * Math.pow(16, i);
					break;
				case 'C':
				case 'c':
					num += 12 * Math.pow(16, i);
					break;
				case 'D':
				case 'd':
					num += 13 * Math.pow(16, i);
					break;
				case 'E':
				case 'e':
					num += 14 * Math.pow(16, i);
					break;
				case 'F':
				case 'f':
					num += 15 * Math.pow(16, i);
					break;
				}
			}
		}
		return num;
	}

}
