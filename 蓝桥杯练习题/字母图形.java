package 蓝桥杯练习题;

/*
 * 问题描述
利用字母可以组成一些美丽的图形，下面给出了一个例子：

ABCDEFG
BABCDEF
CBABCDE
DCBABCD
EDCBABC

这是一个5行7列的图形，请找出这个图形的规律，并输出一个n行m列的图形。

输入格式
输入一行，包含两个整数n和m，分别表示你要输出的图形的行数的列数。
输出格式
输出n行，每个m个字符，为你的图形。

样例输入
5 7
样例输出

ABCDEFG
BABCDEF
CBABCDE
DCBABCD
EDCBABC

数据规模与约定
1 <= n, m <= 26。
 */
import java.util.*;
public class 字母图形 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int row = sc.nextInt();
		int column = sc.nextInt();
		int max = row > column ? row : column;				//得到行列的最大值才得到字母序列
		char[] letter = new char[max];
		char c = 'A';
		for(int i = 0; i < letter.length; i++) {			//将英语字母送入字符数组中
			letter[i] = c++;
		}
		printGraph(row, column, letter);
	}
	//打印特殊字母图形
	public static void printGraph(int row, int column, char[] letter) {
		for(int i = 0; i < row; i++) {
			for(int j = i, k = 1; j >= 0 && k <= column; j--, k++)						//打印每行 A 字母前边的字母
				System.out.print(letter[j]);
			for(int j = 1; j < column-i; j++)				//打印每行 A 字母后边的字母
				System.out.print(letter[j]);
			System.out.println();
		}
	}
	

}
