package 蓝桥杯练习题;

/*
 * 问题描述
　　123321是一个非常特殊的数，它从左边读和从右边读是一样的。
　　输入一个正整数n， 编程求所有这样的五位和六位十进制数，满足各位数字之和等于n 。
输入格式
　　输入一行，包含一个正整数n。
输出格式
　　按从小到大的顺序输出满足条件的整数，每个整数占一行。
样例输入
52
样例输出
899998
989989
998899
数据规模和约定
　　1<=n<=54。
 */
import java.util.*;
public class 特殊回文数 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		printPalindrome(n);
	}
	public static void printPalindrome(int n) {
		for(int i = 10000; i < 1000000; i++) {
			if(isPalindrome(i, n))
				System.out.println(i);
		}
	}
	
	public static boolean isPalindrome(int num, int n) {
		int sum = 0, temp = num, s = 0;
		while(temp > 0) {
			s += temp%10;					//统计每个位数上的值进行累加
			sum = sum*10 + temp%10;			//得到此数的反序数
			temp /= 10;
		}
		if(sum == num && s == n) return true;
		else return false;
	}

}
