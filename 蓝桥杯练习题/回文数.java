package 蓝桥杯练习题;

/*
 * 问题描述
　　1221是一个非常特殊的数，它从左边读和从右边读是一样的，编程求所有这样的四位十进制数。
输出格式
　　按从小到大的顺序输出满足条件的四位十进制数。
 */
public class 回文数 {
	public static void main(String[] args) {
		for(int i = 1000; i < 10000; i++) {
			if(isPalindrome(i))
				System.out.println(i);
		}
	}
	public static boolean isPalindrome(int n) {
		int sum = 0, temp = n;
		while(temp > 0) {
			sum = sum*10 + temp%10;
			temp /= 10;
		}
		if(sum == n) return true;
		else return false;
	}
}
