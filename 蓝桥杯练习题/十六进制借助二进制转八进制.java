package 蓝桥杯练习题;

/*
 * 问题描述
　　给定n个十六进制正整数，输出它们对应的八进制数。

输入格式
　　输入的第一行为一个正整数n （1<=n<=10）。
　　接下来n行，每行一个由0~9、大写字母A~F组成的字符串，表示要转换的十六进制正整数，每个十六进制数长度不超过100000。

输出格式
　　输出n行，每行为输入对应的八进制正整数。

　　【注意】
　　输入的十六进制数不会有前导0，比如012A。
　　输出的八进制数也不能有前导0。

样例输入
　　2
　　39
　　123ABC

样例输出
　　71
　　4435274

　　【提示】
　　先将十六进制数转换成某进制数，再由某进制数转换成八进制。
 */
/*
 * 规律
 * (二进制与八进制之间的关系表达式每个八进制位对应3个二进制位):
 * 八进制:  0   1   2   3   4   5   6   7
 * 二进制:000 001 010 011 100 101 110 111
 * (二进制与十六进制之间的关系表达式每个十六进制位对应4个二进制位):
 * 十六进制:    0    1    2    3    4    5    6    7    8    9    A    B    C    D    E    F
 * 二进制:   0000 0001 0010 0011 0100 0101 0110 0111 1000 1001 1010 1011 1100 1101 1110 1111
 */
import java.util.*;
public class 十六进制借助二进制转八进制 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		while(n-->0) {
			String hex = sc.next();
			System.out.println(getOctal(hex));
		}
		
	}
	public static String getOctal(String hex) {
		String hex_chars = "";
		String otc_chars = "";
		int i = 0;
		for(i = 0; i < hex.length(); i++) {
			switch(hex.charAt(i)) {
			case '0':
				hex_chars += "0000";
				break;
			case '1':
				hex_chars += "0001";
				break;
			case '2':
				hex_chars += "0010";
				break;
			case '3':
				hex_chars += "0011";
				break;
			case '4':
				hex_chars += "0100";
				break;
			case '5':
				hex_chars += "0101";
				break;
			case '6':
				hex_chars += "0110";
				break;
			case '7':
				hex_chars += "0111";
				break;
			case '8':
				hex_chars += "1000";
				break;
			case '9':
				hex_chars += "1001";
				break;
			case 'A':
			case 'a':
				hex_chars += "1010";
				break;
			case 'B':
			case 'b':
				hex_chars += "1011";
				break;
			case 'C':
			case 'c':
				hex_chars += "1100";
				break;
			case 'D':
			case 'd':
				hex_chars += "1101";
				break;
			case 'E':
			case 'e':
				hex_chars += "1110";
				break;
			case 'F':
			case 'f':
				hex_chars += "1111";
				break;
			}
		}
		//若长度不够, 前位补 0
		if(hex_chars.length() % 3 == 1) {
			hex_chars = "00" + hex_chars;
		}else if(hex_chars.length() % 3 ==2) {
			hex_chars = "0" + hex_chars;
		}
		i = 0;
		while(i < hex_chars.length()) {
			String s = hex_chars.substring(i, i+3);
			i += 3;
			switch(s) {
			case "000":
				otc_chars += "0";
				break;
			case "001":
				otc_chars += "1";
				break;
			case "010":
				otc_chars += "2";
				break;
			case "011":
				otc_chars += "3";
				break;
			case "100":
				otc_chars += "4";
				break;
			case "101":
				otc_chars += "5";
				break;
			case "110":
				otc_chars += "6";
				break;
			case "111":
				otc_chars += "7";
				break;
			}
		}
		return otc_chars;
	}

}
