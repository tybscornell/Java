package 牛客网;

/*
 * 链接：https://ac.nowcoder.com/acm/problem/21469
来源：牛客网

题目描述 
凯刚写了一篇美妙的作文，请问这篇作文的标题中有多少个字符？
注意：标题中可能包含大、小写英文字母、数字字符、空格和换行符。统计标题字 符数时，空格和换行符不计算在内。
输入描述:
输入文件只有一行， 一个字符串s。
输出描述:
输出文件只有一行，包含一个整数，即作文标题的字符数（不含空格和换行符）。
示例1
输入
复制
234
输出
复制
3
说明
标题中共有 3 个字符，这 3 个字符都是数字字符。
示例2
输入
复制
Ca 45
输出
复制
4
说明
标题中共有 5 个字符，包括 1 个大写英文字母，1 个小写英文字母和 2 个数字字符，
还有 1 个空格。由于空格不计入结果中，故标题的有效字符数为 4 个。
备注:
规定|s|表示字符串s的长度（即字符串中的字符串中的字符和空格数）。
对于40%的数据，1≤|s|≤5，保证输入为数字符及行末换符。
对于80%的数据，1≤|s|≤5，输入只可能包含大、小写英文字母大、小写英文字母、数字符及行末换符。
对于100%的数据，1≤|s|≤5，输入可能包含大，输入可能包含大、小写英文字母写英文字母、数字符、空格和行末换符。
 */
import java.util.*;
public class 标题统计 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("请输入字符串:");
		String s = sc.nextLine();
		int sum = 0;
		//空格的ASCLL码为 32, 换行符的ASCLL码为 10
		for(int i = 0; i < s.length(); i++) {
			if(s.charAt(i) != 10 && s.charAt(i) != 32)
				sum++;
		}
		//输出总数
		System.out.println(sum);
	}

}
