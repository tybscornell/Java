package 牛客网;

/*
 * 题目描述
假定我们都知道非常高效的算法来检查一个单词是否为其他字符串的子串。请将这个算法编写成一个函数，
给定两个字符串s1和s2，请编写代码检查s2是否为s1旋转而成，要求只能调用一次检查子串的函数。

给定两个字符串s1,s2,请返回bool值代表s2是否由s1旋转而成。字符串中字符为英文字母和空格，区分大小写，
字符串长度小于等于1000。

测试样例：
"Hello world","worldhello "
返回：false
"waterbottle","erbottlewat"
返回：true
 */
import java.util.*;
public class 翻转字串 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Please enter two Strings:");
		String s1 = sc.nextLine();
		String s2 = sc.nextLine();
		System.out.println(checkReverseEqual(s1, s2));
	}
	public static boolean checkReverseEqual(String s1, String s2) {
        // write code here
		int i = 0, j = 0;
		if(s1.length() != s2.length()) return false;	//字符串长度不同,返回false
		for(; i < s2.length(); i++) {		//检索s2中的每一个字符
			int temp = i;			//保留 i 的值		
			while(s1.charAt(j) == s2.charAt(temp)) {    //找到s2中和s1第一个字符匹配的字符
				j++;								//进行j累加循环匹配
				if(j == s1.length()) return true;	//s1与s2的所有字符匹配 , 返回 true
				temp = (temp+1) % s2.length();		//temp 也进行累加匹配
			}
			j = 0;			//匹配不成功,从s2第二个出现和s1的第一个字符相同的字符开始匹配
		}
		return false;		//s2串检索完依旧未成功 , 返回 false;
    }
}
