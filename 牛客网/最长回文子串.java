package 牛客网;

/*
 * 链接：https://ac.nowcoder.com/acm/problem/14517


来源：牛客网

题目描述 

 既然大家都知道回文串是怎么回事了，那我们就长话短说，现在有一个字符串，长度小于1200
 我想知道最长的回文子串长度是多少。 
输入描述:
多组输入，输入字符串只包含小写字母。
输出描述:
每组输出一个数字，表示最长的回文子串。
示例1
输入
复制
aqppqole
ebcml
输出
复制
4
1
 */
//思想: 善于调用方法完成各个部分的功能, 完成各个模块的功能
import java.util.*;
public class 最长回文子串 {
	public static void main(String[] args) {
		Scanner  sc = new Scanner(System.in);
		String s = null;
		while(sc.hasNext()) {
			s = sc.nextLine();
			System.out.println(getMaxSubString(s));
		}
	}
	public static int getMaxSubString(String s) {
		if(isHuiwen(s)) return s.length();
		int start = 0, max = 1;
		while(start < s.length() - max) {
			for(int temp = s.length(); temp - max >= start; temp--) {
				String s1 = s.substring(start, temp);
				if(isHuiwen(s1)) 
					if(s1.length() > max)
						max = s1.length();
			}
			start++;
		}
		return max;
	}
	public static boolean isHuiwen(String s) {
		int i = 0;
		for(; i < s.length()/2; i++) {
			if(s.charAt(i) != s.charAt(s.length() - i - 1)) break;
		}
		if(i == s.length()/2) return true;
		else return false;
	}
}
