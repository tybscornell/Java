package 牛客网;

/*
 * 链接：https://ac.nowcoder.com/acm/problem/16422
来源：牛客网

题目描述 
图书馆中每本书都有一个图书编码，可以用于快速检索图书，这个图书编码是一个正整数。

每位借书的读者手中有一个需求码，这个需求码也是一个正整数。如果一本书的图书编码恰好以读者的需求码结尾，
那么这本书就是这位读者所需要的。

小 D 刚刚当上图书馆的管理员，她知道图书馆里所有书的图书编码，她请你帮她写一个程序，对于每一位读者，
求出他所需要的书中图书编码最小的那本书，如果没有他需要的书，请输出-1。

输入描述:
输入的第一行，包含两个正整数 n 和 q，以一个空格分开，分别代表图书馆里书的数量和读者的数量。
接下来的 n 行，每行包含一个正整数，代表图书馆里某本书的图书编码。
接下来的 q 行，每行包含两个正整数，以一个空格分开，第一个正整数代表图书馆里读者的需求码的长度，
第二个正整数代表读者的需求码。
输出描述:
输出有 q 行，每行包含一个整数，如果存在第 i 个读者所需要的书，
则在第 i 行输出第 i 个读者所需要的书中图书编码最小的那本书的图书编码，否则输出-1。
示例1
输入
复制
5 5
2123
1123
23
24
24
2 23
3 123
3 124
2 12
2 12
输出
复制
23
1123
-1
-1
-1
说明
第一位读者需要的书有 2123、1123、23，其中 23 是最小的图书编码。
第二位读者需要的书有 2123、1123，其中 1123 是最小的图书编码。
对于第三位，第四位和第五位读者，没有书的图书编码以他们的需求码结尾，即没有他们需要的书，输出-1。
备注:
对于 20%的数据，1 ≤ n ≤ 2。  另有 20%的数据，q= 1。
另有 20%的数据，所有读者的需求码的长度均为1。
另有 20%的数据，所有的图书编码按从小到大的顺序给出。
对于 100%的数据，1≤n ≤1,000，1 ≤ q ≤ 1,000，所有的图书编码和需求码均不超过 10,000,000。
 */

import java.util.*;
public class 图书管理员 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		//调用函数输出所有的结果
		printAllResult();
	}
	
//	输出所有读者的检索信息
	public static void printAllResult() {
		Scanner sc = new Scanner(System.in);
//		System.out.println("请输入图书馆藏书的总数目和读者的人数:");
		int n = sc.nextInt();
		int q = sc.nextInt();
		//输入图书馆藏书的信息和读者的信息
		int[] lib = new int[n];
		int[][] stu = new int[q][2];
//		System.out.println("请输入图书馆的所有藏书编号:");
		for(int i = 0; i < n; i++) {
			lib[i] = sc.nextInt();
		}
//		System.out.println("请输入所有学生需求码的长度和需求码:");
		for(int i = 0; i < q; i++) {
			stu[i][0] = sc.nextInt();
			stu[i][1] = sc.nextInt();
		}
		
//		输出每一个读者查询的结果
		for(int i = 0; i < q; i++) {
			System.out.println(getResult(lib, stu[i][0], stu[i][1]));
		}
	}
	
	//求一个读者是否能够找到他所需的编号最小的图书
	public static int getResult(int[] lib, int len, int num) {
		int min = 0;
		boolean flag = false;
		for(int i = 0; i < lib.length; i++) {
			//若 flag == false,且符合读者条件, min有初值 lib[i]
			if(!flag && isAccord(lib[i], len, num)) {
				min = lib[i];
				flag = true;
			}
			//若仍符合条件,且不是初值,进行比较判断
			else if(isAccord(lib[i], len, num) && lib[i] <= min)
				min = lib[i];
		}
		if(flag == false) return -1;	//若标识符未进行改变,返回-1,未找到
		else return min;	//否则返回最小值
	}
	
	//判断读者的取书码是否与图书馆的某书取书码匹配, 返回boolean值
	public static boolean isAccord(int lib, int len, int num) {
		String s = String.valueOf(lib);
		if(s.length() < len) return false;
		//判断数lib的后len部分是否与数num相等,若相等说明匹配成功,否则匹配失败
		
		if(Integer.parseInt(s.substring(s.length() - len, s.length())) == num)
			return true;
		else return false;
	}

}
