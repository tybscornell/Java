package 牛客网;

/*
 * 链接：https://ac.nowcoder.com/acm/problem/16585

来源：牛客网

题目描述 
一般的文本编辑器都有查找单词的功能，该功能可以快速定位特定单词在文章中的位置，
有的还能统计出特定单词在文章中出现的次数。
现在，请你编程实现这一功能，具体要求是：给定一个单词，请你输出它在给定的文章中出现的次数和第一次出现的位置。
注意：匹配单词时，不区分大小写，但要求完全匹配，即给定单词必须与文章
中的某一独立单词在不区分大小写的情况下完全相同（参见样例1 ），
如果给定单词仅是文章中某一单词的一部分则不算匹配（参见样例2 ）
输入描述:
共 2 行。
第 1 行为一个字符串，其中只含字母，表示给定单词；
第 2 行为一个字符串，其中只可能包含字母和空格，表示给定的文章
输出描述:
一行，如果在文章中找到给定单词则输出两个整数，两个整数之间用一个空格隔开，
分别是单词在文章中出现的次数和第一次出现的位置（即在文章中第一次出现时，
单词首字母在文章中的位置，位置从 0 开始）；如果单词在文章中没有出现，则直接输出一个整数 -1。
示例1
输入
复制
To
to be or not to be is a question
输出
复制
2 0
说明
输出结果表示给定的单词 To 在文章中出现两次，第一次出现的位置为0。
示例2
输入
复制
to
Did the Ottoman Empire lose its power at that time
输出
复制
-1
说明
表示给定的单词 to 在文章中没有出现，输出整数-1。
 */
import java.util.*;
public class 统计单词数 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
//		System.out.println("请输入需要统计的单词 和 待统计的文章:");
		String s = sc.nextLine();
		String article = sc.nextLine();
		System.out.println(getCount(s, article));
	}
	public static int getCount(String word, String s) {
		int count = 0;
		//将单词和字符串首先都转化为小写
		word = word.toLowerCase();
		s = s.toLowerCase();
		//将文章分解为单词数组
		String[] words = s.split(" ");
		//统计单词个数
		for(int i = 0; i < words.length; i++) {
			if(word.equals(words[i])) count++;
		}
		if(count != 0) return count;
		else return -1;
	}

}
