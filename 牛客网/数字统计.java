package 牛客网;

/*
 * 链接：https://ac.nowcoder.com/acm/problem/16599
来源：牛客网

题目描述 
请统计某个给定范围[L, R]的所有整数中，数字2出现的次数。

比如给定范围[2, 22]，数字2在数2中出现了1次，在数12中出现1次，在数20中出现1次，在数21中出现1次
在数22中出现2次，所以数字2在该范围内一共出现了6次。

输入描述:
输入共1行，为两个正整数L和R，之间用一个空格隔开。
输出描述:
输出共1行，表示数字2出现的次数。
示例1
输入
复制
2 22
输出
复制
6
示例2
输入
复制
2 100
输出
复制
20
备注:
1≤L≤R≤10000。
 */
import java.util.*;
public class 数字统计 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int l, r;
//		System.out.println("请输入统计范围的下限和上限:");
		l = sc.nextInt();
		r = sc.nextInt();
		System.out.println(sumCount(l, r));
	}
	
	//得出 l 到 r 所有数中 '2' 出现的次数
	public static int sumCount(int l, int r) {
		int sum = 0;
		for(int i = l; i <= r; i++) {
			sum += numCount(i);		//判断每一个数中2数字出现的次数
		}
		return sum;
	}
	
	//获取一个数字中出现 2 的次数
	public static int numCount(int m) {
		int sum = 0;
		String s = String.valueOf(m);	//将此数字转化为字符串,判断数字2出现的次数
		for(int i = 0; i < s.length(); i++) {
			if(s.charAt(i) == '2') sum++;
		}
		return sum;
	}

}
