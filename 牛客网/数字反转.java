package 牛客网;

/*
 * 链接：https://ac.nowcoder.com/acm/problem/16584
来源：牛客网

题目描述 
给定一个整数，请将该数各个位上数字反转得到一个新数。新数也应满足整数的常见形式
即除非给定的原数为零，否则反转后得到的新数的最高位数字不应为零（参见样例2）。
输入描述:
一个整数 N。
输出描述:
一个整数，表示反转后的新数。
示例1
输入
复制
123
输出
复制
321
示例2
输入
复制
-380
输出
复制
-83
备注:
-1,000,000,000≤N≤1,000,000,000
 */
import java.util.*;
public class 数字反转 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
//		System.out.println("请输入一个数:");
		int n = sc.nextInt();
		System.out.println(getTurnNum(n));	//调用方法返回反序数
	}
	public static int getTurnNum(int n) {
		int temp = Math.abs(n), sum = 0;
		while(temp > 0) {		//求一个数的反序数
			sum = sum*10 + temp%10;
			temp /= 10;
		}
		if(n >= 0) return sum;	//判断原数的正负
		else return -sum; 
	}

}
