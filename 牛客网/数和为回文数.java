package 牛客网;

/*

题目描述 
若一个数（首位不为零）从左向右读与从右向左读都一样，我们就将其称之为回文数。

例如：给定一个10进制数56，将56加56（即把56从右向左读），得到121是一个回文数。
又如：对于10进制数87：
STEP1：87+78  = 165                  STEP2：165+561 = 726

STEP3：726+627 = 1353                STEP4：1353+3531 = 4884

在这里的一步是指进行了一次N进制的加法，上例最少用了4步得到回文数4884。

写一个程序，给定一个N（2<=N<=10，N=16）进制数M，求最少经过几步可以得到回文数。
如果在30步以内（包含30步）不可能得到回文数，则输出“Impossible！”
输入描述:
两行，分别是N，M。
输出描述:
STEP=ans(ans表示答案)
示例1
输入
复制
9
87
输出
复制
STEP=6
 */

//善于使用方法, 模块化解决问题
import java.util.*;
public class 数和为回文数 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n, m, sum, i;
		boolean flag = false;		//设置成功标志
		System.out.println("请输入这个N(2 <= N <= 10)进制数 M的N和M:");
		n = sc.nextInt();
		m = sc.nextInt();		//输入 n, m
		sum = turnNum(n, m);		//将输入的N进制数M转化为10进制数 sum
		for(i = 1; i <= 30; i++) {
			sum += getHuiwenNum(sum);		//将sum和它的反序数相加赋值给sum
			if(sum == getHuiwenNum(sum)) {		//判断sum是否和它的反序数相等,若相等,即为回文
				System.out.println("STEP=" + i);	//输出信息
				flag = true;		//标志位变为true
				break;	//退出
			}
		}
		if(!flag) System.out.println("Impossible!");	//若未成功,输出信息
	}
	
	//将 n 进制数 m, 统一转化为 10 进制数
	public static int turnNum(int n, int m) {
		int temp = m, i = 0;
		double sum = 0;
		while(temp > 0) {		//将N进制数M转化为 10进制数,并返回
			sum += temp%10 * Math.pow((double)n, i);
			temp /= 10;
			i++;
		}
		return (int)sum;
	}
	
	//此方法为获取一个数的回文数(反序数)
	public static int getHuiwenNum(int num) {
		int temp = num, sum = 0;
		while(temp > 0) {	//得到num这个数的反序数
			sum = sum*10 + temp%10;
			temp /= 10;
		}
		return sum;
	}
	

}
