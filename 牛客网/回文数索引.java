package 牛客网;

/*
 * 题目描述
给定一个仅由小写字母组成的字符串。现在请找出一个位置，删掉那个字母之后，字符串变成回文。请放心总会有一个合法的解。如果给定的字符串已经是一个回文串，那么输出-1。
输入描述:
第一行包含T，测试数据的组数。后面跟有T行，每行包含一个字符串。
输出描述:
如果可以删去一个字母使它变成回文串，则输出任意一个满足条件的删去字母的位置（下标从0开始）。例如：

bcc

我们可以删掉位置0的b字符。
示例1
输入
复制
3
aaab
baa
aaa
输出
复制
3
0
-1
 */
import java.util.*;
public class 回文数索引 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		sc.nextLine();
		String s = null;
		for(int i = 1; i <= n; i++) {
			s = sc.nextLine();
			System.out.println(getHuiwen(s));
		}
	}
	
	//善于使用方法模块化解决问题
	//判断一个字符串是否为回文字符串的方法
	public static boolean isHuiwen(String s) {
		if(s.length() == 0) return false;
		int i = 0;
		for(; i < s.length()/2; i++)
			if(s.charAt(i) != s.charAt(s.length()-1-i))
				break;
		if(i >= s.length()/2) return true;
		else return false;
	}
	
	//得到合适的删除位置所生成的回文字符串
	public static int getHuiwen(String s) {
		if(isHuiwen(s)) return -1;		//若原数据为回文串, 即返回 -1
		for(int i = 0; i < s.length(); i++) {
			String temp = null;
			//String类两个方法进行字符串的截取和连接形成新的字符串
			temp = s.substring(0, i);	//截取 i 位置之前的串
			temp = temp.concat(s.substring(i+1, s.length())); //将 i 位置之后的串连接至新串
			if(isHuiwen(temp)) return i;	//判断是否回文
		}
		return -1;
	}

}
