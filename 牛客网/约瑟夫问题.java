package 牛客网;

import java.util.*;
public class 约瑟夫问题 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n, m;
		System.out.println("请输入总的人数和出圈范围数：");
		n = sc.nextInt();
		m = sc.nextInt();
		System.out.println("出圈的最后一个人为：\n" + (getLast(n, m) + 1));
		
	}
	public static int getLast(int n, int m) {
		if(n == 1) return 0;			//如果只剩下一个人,序号为1,返回0相当于下标
		else {
			return (getLast(n-1, m) + m) % n;		//否则进行递推
		}
	}
/*  int cir(int n,int m)		//非递归算法返回序号
	{
		int p=0;
		for(int i=2;i<=n;i++)
		{
			p=(p+m)%i;
		}
		return p+1;
	}					*/

}
