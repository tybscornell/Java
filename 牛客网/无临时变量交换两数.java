package 牛客网;

//不使用额外的变量交换两个数的值
import java.util.*;
public class 无临时变量交换两数 {
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("请输入这两个数的值:");
		int a = sc.nextInt();
		int b = sc.nextInt();
		System.out.println("交换前两个数的值如下:\n" + a + " " + b);
		//两个数进行交换
//		b = a - b;
//		a = a - b;
//		b = a + b;
		//或者
		b = a ^ b;
		a = a ^ b;
		b = a ^ b;
		System.out.println("交换后两个数的值如下:\n" + a + " " + b);
	}

}
