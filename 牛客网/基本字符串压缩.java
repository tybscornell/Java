package 牛客网;

/*
 * 题目描述
利用字符重复出现的次数，编写一个方法，实现基本的字符串压缩功能。
比如，字符串“aabcccccaaa”经压缩会变成“a2b1c5a3”。若压缩后的字符串没有变短，则返回原先的字符串。

给定一个string iniString为待压缩的串(长度小于等于10000)，保证串内字符均由大小写英文字母组成，返回一个string，
为所求的压缩后或未变化的串。

测试样例
"aabcccccaaa"
返回："a2b1c5a3"
"welcometonowcoderrrrr"
返回："welcometonowcoderrrrr"
 */
import java.util.*;
public class 基本字符串压缩 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Please enter a String:");
		String s = sc.nextLine();
		System.out.println(zipString(s));
	}
	public static String zipString(String iniString) {
        // write code here
		char[] c = new char[10000];			//设置字符数组大小为10000
		int start = 0, k = 0, j = 0, m;		//定义主串检索的开始start,新串下标k,旧串下标j,数字转串的下标m
		while(j < iniString.length()) {		//当旧串扫描完时退出
			int i = 0;			//字符相同的计数器
			String num = null;	//存放数字的字符串
			c[k] = iniString.charAt(start);			//把每次主串的第一个不同的字符由新串存储
			while(c[k] == iniString.charAt(j)) {
				i++;
				j++;							//循环累加求相同字符的个数
				if(j == iniString.length()) break;
			}
			num = String.valueOf(i);
			for(m = 0, k +=1; m < num.length(); m++) 	//将数字转化为串并存入新串
				c[k++] = num.charAt(m);
			start += i;			//主串检索下标累加 i, 从下一个不同的字符继续开始扫描
		}
		if(new String(c, 0, k).length() >= iniString.length()) return iniString;
		else return new String(c, 0, k);		//得出结果
    }

}
