package 牛客网;
/*
 * 题目描述
有一个介于0和1之间的实数，类型为double，返回它的二进制表示。
如果该数字无法精确地用32位以内的二进制表示,返回“Error”。

给定一个double num，表示0到1的实数，请返回一个string，代表该数的二进制表示或者“Error”。

测试样例：
0.625
返回：0.101
 */
import java.util.*;
public class 二进制小数 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double num = sc.nextDouble();
		System.out.println(printBin(num));
	}
	public static String printBin(double num) {
        // write code here
		int i = 2;
		char[] c = new char[34];		//字符串最长为 34个字符
		c[0] = '0';			
		c[1] = '.';			//确定开始字符
		while(i <= 33) {
			num *= 2;		//累乘
			if(num > 1) {				//若结果大于1
				num -= 1;				//保留小数部分
				c[i++] = '1';				//存取整数部分
			}else if(num == 1) {		//若结果大于0
				c[i++] = '1';		//存取结果
				break;			//退出循环
			}
			else c[i++] = '0';		//保留整数部位
		}
		//若 num 为 1 ,成立,返回字符数组第 0 个字符至 第 i 个字符组成的字符串
		if(num == 1) return new String(c,0,i);	
		else return new String("Error");
    }

}
