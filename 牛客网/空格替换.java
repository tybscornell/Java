package 牛客网;

/*
 * 题目描述
请编写一个方法，将字符串中的空格全部替换为“%20”。假定该字符串有足够的空间存放新增的字符
并且知道字符串的真实长度(小于等于1000)，同时保证字符串由大小写的英文字母组成。

给定一个string iniString 为原始的串，以及串的长度 int len, 返回替换后的string。

测试样例：
"Mr John Smith”,13
返回："Mr%20John%20Smith"
”Hello  World”,12
返回：”Hello%20%20World”
 */
import java.util.*;
public class 空格替换 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String s = sc.nextLine();
		System.out.println(replaceSpace(s, s.length()));
	}
	public static String replaceSpace(String iniString, int length) {
        // write code here
		char[] c = null;
		int i = 0;
		for(int j = 0; j < iniString.length(); j++)
			if(iniString.charAt(j) == ' ') i++;		//统计空格的数目
		c = new char[iniString.length() + 2*i];		//开辟新的字符数组待转字符串
		for(int j = 0, k = 0; j < c.length; j++, k++) {
			if(iniString.charAt(k) == ' ') {	//若为空格则赋值 串'%20'
				c[j++] = '%';
				c[j++] = '2';
				c[j] = '0';
			}else {
				c[j] = iniString.charAt(k);		//不为空格则 依次复制
			}
		}
		return new String(c);		//返回字符数组转化的字符串
    }

}
