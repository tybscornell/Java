package 牛客网;

/*
 * 题目描述
有两个32位整数n和m，请编写算法将m的二进制数位插入到n的二进制的第j到第i位,其中二进制的位数从低位数到高位且以0开始。

给定两个数int n和int m，同时给定int j和int i，意义如题所述，请返回操作后的数
保证n的第j到第i位均为零，且m的二进制位数小于等于i-j+1。

测试样例：
1024，19，2，6
返回：1100
 */
import java.util.*;
public class 二进制插入 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n, m, j, i;
		System.out.println("请输入整数 n 和 m , 插入的位置 j 和 i:");
		n = sc.nextInt();
		m = sc.nextInt();
		j = sc.nextInt();
		i = sc.nextInt();
		System.out.println(binInsert(n, m, j, i));
	}
	public static int binInsert(int n, int m, int j, int i) {
        // write code here
		m <<= j;
		n |= m;
        return n;
    }
	/*
	 * 思想：
	 * 		1024的二进制数为: 0000 0100 0000 0000
	 * 		19    的二进制数为: 0000 0000 0001 0011
	 * 若将 19的二进制插入到1024的2到6的位置的二进制数得:
	 * 						 0000 0100 0100 1100
	 * 					即为:  1100
	 * 可以看作
	 * 			19 二进制数左移 j位后 和 n 进行按位或操作, 即解决.
	 */

}
