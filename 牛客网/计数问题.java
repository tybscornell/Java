package 牛客网;

/*
 * 链接：https://ac.nowcoder.com/acm/problem/16538
来源：牛客网

题目描述 
试计算在区间1 到n 的所有整数中，数字x（0 ≤ x ≤ 9）共出现了多少次？
例如，在1到11 中，即在1、2、3、4、5、6、7、8、9、10、11 中，数字1 出现了4 次。
输入描述:
输入共1行，包含2个整数n、x，之间用一个空格隔开。
输出描述:
输出共1行，包含一个整数，表示x出现的次数。
示例1
输入
复制
11 1
输出
复制
4
备注:
对于100%的数据，1≤ n ≤ 1,000,000，0 ≤ x ≤ 9。
 */
import java.util.*;
public class 计数问题 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
//		System.out.println("请输入区间 n 和 待统计的数字 x :");
		int n, x;
		n = sc.nextInt();
		x = sc.nextInt();
		System.out.println(sumCount(n, x));
	}
	
	//得出 1 到 n 所有数中 x 出现的次数
	public static int sumCount(int n, int x) {
		int sum = 0;
		for(int i = 1; i <= n; i++) {
			sum += numCount(i, x);		//判断每一个数中x数字出现的次数
		}
		return sum;
	}
	
	//判断每一个数字中包含的数字x的总数
	public static int numCount(int m, int x) {
		int sum = 0;
		char c = (char)(x + '0');
		String s = String.valueOf(m);	//将此数字转化为字符串,判断数字x出现的次数
		for(int i = 0; i < s.length(); i++) {
			if(s.charAt(i) == c) sum++;
		}
		return sum;
	}

}
