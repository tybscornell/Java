package tree;


//二叉树的基本操作方法
import java.util.*;

public class BinaryTree {
	
	
	//根据先序序列和中序序列构建二叉树
	public TreeNode preBinaryTree(int [] pre,int [] in) {
		TreeNode tree = new TreeNode(pre[0]);		//创建该树
		
		//若数组的长度为 1 , 说明创建完毕, 说明此树已无左右子树
		if(pre.length == 1 && in.length ==1){
			tree.left = null;
			tree.right = null;
			return tree;
		}
		
		//检索先序序列的第一个根子树在中序序列的位置
		int first = indexValue(in, pre[0]);
		
		//若在中序的位置为 0, 说明此树无左子树
		if(first == 0){
			int[] new_pre = getArray(pre,1,pre.length);
			int[] new_in = getArray(in,1,in.length);		//创建子序列
			
			//递归调用创建左右子树
			tree.left = null;
			tree.right = preBinaryTree(new_pre,new_in);

		//若在中序的位置为 最后一个位置length-1, 说明此树无右子树
		}else if(first == in.length-1){
			int[] new_pre = getArray(pre,1,pre.length);
			int[] new_in = getArray(in,0,in.length-1);		//创建子序列
			
			//递归调用创建左右子树
			tree.left = preBinaryTree(new_pre,new_in);
			tree.right = null;
			
		//若在中序的中间任一位置
		}else{
			int[] pre_left = getArray(pre,1,first+1);
			int[] in_left = getArray(in, 0, first);					//创建此树的左子树序列
			
			int[] pre_right = getArray(pre,first+1,pre.length);
			int[] in_right = getArray(in, first+1, in.length);		//创建此树的右子树序列
			
			//递归调用创建左右子树
			tree.left = preBinaryTree(pre_left,in_left);
			tree.right = preBinaryTree(pre_right,in_right);
		}
		
        return tree;	//返回树根
    }
	
	
	//根据树的后序序列和中序序列构造一棵二叉树
	public TreeNode postBinaryTree(int[] post, int[] in) {
		//创建该树的树根
		TreeNode tree = new TreeNode(post[post.length-1]);
		
		//若序列长度均为1, 则创建树根并返回
		if(post.length == 1 && in.length == 1) {
			tree.left = null;
			tree.right = null;
			return tree;
		}
		
		//检索先序序列的第一个根子树在中序序列的位置
		int first = indexValue(in, post[post.length-1]);
		
		//若在中序的位置为 0, 说明此树无左子树
		if(first == 0){
			int[] new_post = getArray(post,0,post.length-1);
			int[] new_in = getArray(in,1,in.length);		//创建子序列
					
			//递归调用创建左右子树
			tree.left = null;
			tree.right = postBinaryTree(new_post, new_in);

			//若在中序的位置为 最后一个位置length-1, 说明此树无右子树
		}else if(first == in.length-1){
			int[] new_post = getArray(post,0,post.length-1);
			int[] new_in = getArray(in,0,in.length-1);		//创建子序列
			
			//递归调用创建左右子树
			tree.left = postBinaryTree(new_post,new_in);
			tree.right = null;
			
		//若在中序的中间任一位置
		}else{
			int[] post_left = getArray(post,0, first);
			int[] in_left = getArray(in, 0, first);					//创建此树的左子树序列
			
			int[] post_right = getArray(post, first, post.length-1);
			int[] in_right = getArray(in, first+1, in.length);		//创建此树的右子树序列
			
			//递归调用创建左右子树
			tree.left = postBinaryTree(post_left, in_left);
			tree.right = postBinaryTree(post_right, in_right);
		}
		
		//返回树根
		return tree;
		
	}
	
	
	//先序遍历二叉树
	public void preSearch(TreeNode root) {
		if(root == null) return;
		else {
			System.out.println(root.val);		//访问根节点
			preSearch(root.left);				//先序遍历左子树
			preSearch(root.right);				//先序遍历右子树
		}
		return;
	}
	
	
	//中序遍历二叉树
	public void inSearch(TreeNode root) {
		if(root == null) return;
		else {
			inSearch(root.left);				//中序遍历左子树
			System.out.println(root.val);		//访问根节点
			inSearch(root.right);				//中序遍历右子树
		}
		return;
	}
	
	
	//后序遍历二叉树
	public void postSearch(TreeNode root) {
		if(root == null) return;
		else {
			postSearch(root.left);				//后序遍历左子树
			postSearch(root.right);				//后序遍历右子树
			System.out.println(root.val);		//访问根节点
		}
		return;
	}
	
	
	
	//检索一个数在整型数组中的值
	public int indexValue(int[] array, int value){
		for(int i = 0; i < array.length; i++){
			if(array[i] == value) return i;
		}
		return -1;
	}
	
	//返回一个数组中的下标从i个到j-1的子数组
	public int[] getArray(int[] array, int i, int j){
		int[] new_array = new int[j-i];
		for(int k = i; k < j; k++){
			new_array[k-i] = array[k];
		}
		return new_array;
	}
	
	//打印数组中的所有数据
	public void printArray(int[] array){
		for(int i = 0; i < array.length; i++) System.out.print(array[i] + " ");
		System.out.println();
	}
	
	//以逆时针旋转 90度打印二叉树
	public void printTree(TreeNode tree, int h){
		if(tree == null) return;
		printTree(tree.right, h+1);
		for(int i = 0; i < h; i++){
			System.out.print("    ");
		}
		System.out.println(tree.val);
		printTree(tree.left, h+1);
	}
	

}

//二叉树结点
class TreeNode{
	int val;
	TreeNode left = null;
	TreeNode right = null;
	public TreeNode(int val){
		this.val = val;
	}
}

