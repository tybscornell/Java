package tree;


import java.util.*;
public class 测试 {
	/*二叉树的基本方法
	 * public TreeNode preBinaryTree(int [] pre,int [] in)		//根据先序序列和中序序列构建二叉树
	 * public TreeNode postBinaryTree(int[] post, int[] in)		//根据树的后序序列和中序序列构造一棵二叉树
	 * public void preSearch(TreeNode root)						//先序遍历二叉树
	 * public void inSearch(TreeNode root)						//中序遍历二叉树
	 * public void postSearch(TreeNode root)					//后序遍历二叉树
	 * public int indexValue(int[] array, int value)			//检索一个数在整型数组中的值
	 * public int[] getArray(int[] array, int i, int j)			//返回一个数组中的下标从i个到j-1的子数组
	 * public void printArray(int[] array)						//打印数组中的所有数据
	 * public void printTree(TreeNode tree, int h)				//以逆时针旋转 90度打印二叉树
	 * 
	*/
	public static void main(String[] args) {
		BinaryTree tree =  new BinaryTree();
		BinaryTree tree2 = new BinaryTree();
		TreeNode root = null;
		TreeNode root2 = null;
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int[] post = new int[n];
		int[] in = new int[n];
		for(int i = 0; i < post.length; i++) post[i] = sc.nextInt();
		for(int i = 0; i < in.length; i++) in[i] = sc.nextInt();
		root = tree.postBinaryTree(post, in);
//		tree.printTree(root, 2);
		int m = sc.nextInt();
		int[] post2 = new int[m];
		int[] in2 = new int[m];
		for(int i = 0; i < post2.length; i++) post2[i] = sc.nextInt();
		for(int i = 0; i < in2.length; i++) in2[i] = sc.nextInt();
		root2 = tree2.postBinaryTree(post2, in2);
		tree.printTree(root, 2);
		tree2.printTree(root2, 2);
		System.out.println(HasSubtree(root, root2));
	}
	
	//判断树root2是否为root1的子树
	public static boolean HasSubtree(TreeNode root1,TreeNode root2) {
	    String s1 = "";
	    String s2 = "";
	    s1 = getSequence(root1, s1);		//获取树root1的先序访问序列
	    s2 = getSequence(root2, s2);		//获取树root2的先序访问序列
	    //如果树root1的访问序列中包含root2的访问序列, 则为子树, 否则不是
	    if(s1.indexOf(s2) != -1) return true;
	    else return false;
	}
			
			
	//将一个二叉树的所有根节点数据利用先序遍历二叉树连接到一个字符串中
	public static String getSequence(TreeNode root, String sequence) {
		if(root == null) return sequence;
		else {
			sequence += Integer.toString(root.val);			//添加根节点数据
			sequence = getSequence(root.left, sequence);	//访问左子树
			sequence = getSequence(root.right, sequence);	//访问右子树
		}
		//返回这个字符串
		return sequence;
	}

}
