package graph;

import java.util.*;

import graph.AdjMatrix.Kind;
public class 测试 {
	//测试邻接矩阵式图的基本操作方法
	/*
	 * public AdjMatrix(int vertex, int arc)					//图的构造方法,参数为顶点个数和弧的个数
	 * private Kind getGraphKind()								//私有方法, 获取图的类型
	 * private char[] inputVertex(char[] vertex)				//私有方法, 输入返回顶点数组
	 * private ArcNode[][] inputArcNode(ArcNode[][] arcs)		//私有方法, 输入返回顶点关系矩阵
	 * public int locateVertex(char c)							//公开方法, 根据顶点取得所在顶点数组的序号
	 * public char getVertex(int i)								//公开方法, 根据顶点数组的序号取得所在顶点
	 * public char FirstAdjVertex(char v)						//返回图 中顶点v的第一个邻接点
	 * public char nextAdjVertex(char v, char w)				//返回顶点 v的邻接点 w 后的第一个邻接点
	 * public void insertVertex(char c)							//向图中增加一个顶点
	 * public void deleteVertex(char v)							//删除图中的一个顶点及其关系的弧
	 * public void insertArc(char v, char w)					//往图中插入一条从顶点v到顶点w的一条弧
	 * public void deleteArc(char v, char w)					//删除图中的一条弧
	 * public void printAllVertex()								//打印此图的顶点数组
	 * public void printAllArcs()								//打印此图的关系矩阵
	 * public void traverseGraph()								//遍历邻接矩阵
	 * public void depthFirstSearch(int i)						//深度优先遍历
	 * public void depthFirstSearchMat(int i)					//邻接矩阵优先遍历
	 */
	public static void main(String[] args) {
		AdjMatrix mat_graph = new AdjMatrix(9, 10);		//生成具有4个顶点4条边的图
		mat_graph.printAllVertex();
		mat_graph.printAllArcs();
		mat_graph.traverseGraph();
		
		
	}

}
