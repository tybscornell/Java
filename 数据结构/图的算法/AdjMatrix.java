package graph;

//以邻接矩阵方式创建存储图
import java.util.*;
public class AdjMatrix {
	Scanner sc = new Scanner(System.in);
	//定义全局常量无穷大
	public static final int INFINITY = 32768;
	public static final int MAX = 100;
	//定义标志数组flag
	boolean[] flag = null;
	//定义图的顶点个数vernum, 边数 arcnum;
	int vernum, arcnum;
	//图的种类 DG: 有向图, UDG: 无向图 ,DN: 有向网, UDN:无向网
	enum Kind{DG, UDG, DN, UDN};
	//图的类型
	Kind kind = null;
	//定义存储顶点信息的数组 和 存储顶点之间关系的二维数组
	char[] vertex = new char[MAX];
	ArcNode[][] arcs = new ArcNode[MAX][MAX];
	//通过构造函数传递此图的顶点数和边数
	public AdjMatrix(int vernum, int arcnum) {
		this.vernum = vernum;
		this.arcnum = arcnum;
		kind = getGraphKind();
		vertex = inputVertex(vertex);
		arcs = inputArcNode(arcs);
		System.out.println("创建此图成功!");
	}
	
	
	//获取图的类型
	private Kind getGraphKind() {
		int i;
		Kind graph_kind = null;
		System.out.println("请输入创建图的种类:");
		System.out.println("1.无向图\t2.有向图\n3.无向网\t4.有向网");
		do {
			i = sc.nextInt();
			switch(i) {
			case 1:graph_kind = kind.UDG;break;
			case 2:graph_kind = kind.DG;break;
			case 3:graph_kind = kind.UDN;break;
			case 4:graph_kind = kind.DN;break;
			default:System.out.println("输入有误, 请重新输入：");break;
			}
		}while(i > 4 || i < 1);
		return graph_kind;
	}
	
	
	//输入顶点信息
	private char[] inputVertex(char[] vertex) {
		for(int i = 0; i < this.vernum; i++) {
			System.out.println("请输入第 " + (i+1) +"个顶点:");
			vertex[i] = sc.next().charAt(0);
		}
		return vertex;
	}
	
	
	//输入并返回弧的信息
	private ArcNode[][] inputArcNode(ArcNode[][] arcs){
		//初始化存储顶点信息的二维数组
		for(int i = 0; i < this.vernum;  i++) {
			for(int j = 0; j < this.vernum; j++) {
				arcs[i][j] = new ArcNode();
				arcs[i][j].start = i+1;
				arcs[i][j].end = j+1;
				arcs[i][j].flag = false;
			}
		}
		//输入弧及边的信息
		for(int i = 0; i < this.arcnum; i++) {
			char start, end;
			int start_int, end_int, weight = this.INFINITY;
			System.out.println("请输入第" + (i+1) + "条边的第一个顶点");
			start = sc.next().charAt(0);
			start_int = locateVertex(start);
			System.out.println("请输入第" + (i+1) + "条边的第二个顶点");
			end = sc.next().charAt(0);
			end_int = locateVertex(end);
			//若是有向网或者无向网, 输入这条边的权值
			if(this.kind == Kind.DN || this.kind == Kind.UDN) {
				System.out.println("请输入这两个顶点构成边的权值:");
				weight = sc.nextInt();
			}
			arcs[start_int-1][end_int-1].flag = true;
			arcs[start_int-1][end_int-1].weight = weight;
			//若为无向图或者无向网, 则这个顶点互为邻接点, 即相通
			if(this.kind == Kind.UDG || this.kind == Kind.UDN) {
				arcs[end_int-1][start_int-1].flag = true;
				arcs[end_int-1][start_int-1].weight = weight;
			}
		}
		return arcs;
	}
	
	
	//检索某个顶点在顶点数组中的位置
	public int locateVertex(char c) {
		if(c == '\0') return 0;
		int i = 0;
		while(i < this.vernum) {
			if(this.vertex[i] == c) return i+1;
			else i++;
		}
		return 0;
	}
	
	
	//获取顶点数组某个位置的数据
	public char getVertex(int i) {
		if(i == 0 || i > this.vernum) return '\0';
		return this.vertex[i-1];
	}
	
	
	//返回图 中顶点v的第一个邻接点
	public char firstAdjVertex(char v) {
		//获取顶点 v 在顶点数组中的序号
		int v_int = locateVertex(v)-1;		//因为要查找下标, 所以-1
		for(int i = 0; i < this.vernum; i++) {
			if(arcs[v_int][i].flag == true)	//若这两点存在关系
				return getVertex(i+1);		//返回此顶点的第一个邻接点
		}
		//未找到返回空
		return '\0';
	}
	
	
	//返回顶点 v的邻接点 w 后的第一个邻接点
	public char nextAdjVertex(char v, char w) {
		int y = locateVertex(v)-1;
		int x = locateVertex(w)-1;
		//若顶点 v已是最后一个邻接点, 返回空
		if(x == this.vernum-1) return '\0';
		for(int i = x+1; i < this.vernum; i++) {
			if(this.arcs[y][i].flag == true)
				return getVertex(i+1);
		}
		//未找到返回空
		return '\0';
	}
	
	
	//向图中增加一个邻接点
	public void insertVertex(char c) {
		this.vertex[this.vernum] = c;
		this.vernum++;
		//新添加顶点后的邻接矩阵变化
		for(int i = 0; i < this.vernum; i++) {
			this.arcs[this.vernum-1][i] = new ArcNode();
			this.arcs[this.vernum-1][i].start = this.vernum-1;
			this.arcs[this.vernum-1][i].end = i;
			this.arcs[this.vernum-1][i].flag = false;
			this.arcs[this.vernum-1][i].weight = this.INFINITY;
		}
		for(int i = 0; i < this.vernum-1; i++) {
			this.arcs[i][this.vernum-1] = new ArcNode();
			this.arcs[i][this.vernum-1].start = this.vernum-1;
			this.arcs[i][this.vernum-1].end = i;
			this.arcs[i][this.vernum-1].flag = false;
			this.arcs[i][this.vernum-1].weight = this.INFINITY;
		}
	}
	
	
	//删除图中顶点 v 以及和它相关的 弧
	public void deleteVertex(char v) {
		int v_int = locateVertex(v)-1;
		if(v_int == this.vernum-1) {
			 this.vernum--;
		}
		else {
			//从下往上覆盖删除 和顶点 v 有关的弧
			for(int i = v_int; i < this.vernum-1; i++) {
				for(int j = 0; j < this.vernum; j++) {
					this.arcs[i][j] =  this.arcs[i+1][j];
				}
			}
			//从右往左覆盖删除 和顶点 v 有关的弧
			for(int i = v_int; i < this.vernum-1; i++) {
				for(int j = 0; j < this.vernum; j++) {
					this.arcs[j][i] =  this.arcs[j][i+1];
				}
			}
			//删除顶点表中的数据
			for(int i = v_int; i < this.vernum-1; i++)
				this.vertex[i] = this.vertex[i+1];
			this.vernum--;
		}
		//将弧数置为 0
		this.arcnum = 0;
		//再次统计弧数
		for(int i = 0; i < this.vernum; i++) {
			for(int j = 0; j < this.vernum; j++) {
				if(this.arcs[i][j].flag == true) {
					this.arcnum++;
				}
			}
		}
		//如果此图为有向图, 弧数为其一半
		if(this.kind == Kind.DG || this.kind == Kind.DN) this.arcnum /= 2;
		return;
	}
	
	
	//往图中插入一条从顶点v到顶点w的一条弧
	public void insertArc(char v, char w) {
		int v_int = locateVertex(v)-1;
		int w_int = locateVertex(w)-1;
		this.arcs[v_int][w_int].flag = true;
		//若此图为无向图或无向网, w到v 的关系也应改为 true
		if(this.kind == Kind.UDG || this.kind == Kind.UDN)
			this.arcs[w_int][v_int].flag = true;
		//弧数累加
		this.arcnum++;
	}
	
	
	//删除图中的一条弧
	public void deleteArc(char v, char w) {
		int v_int = locateVertex(v)-1;
		int w_int = locateVertex(w)-1;
		this.arcs[v_int][w_int].flag = false;
		//若此图为无向图或无向网, w到v 的关系也应改为 false
		if(this.kind == Kind.UDG || this.kind == Kind.UDN)
			this.arcs[w_int][v_int].flag = false;
		//弧数累减
		this.arcnum++;
	}
	
	
	//遍历此图
	public void traverseGraph() {
		this.flag = new boolean[this.vernum];
		//初始化标识数组
		for(int i = 0; i < this.flag.length; i++) flag[i] = false;
		for(int i = 0; i < this.vernum; i++)
			if(!this.flag[i]) depthFirstSearch(i);
	}
	
	
	//从顶点i出发深度优先搜索方法
	public void depthFirstSearch(int i) {
		char c;
		int first = 0;
		//标志变为true, 代表已经访问过此顶点
		this.flag[i] = true;
		c = getVertex(i+1);
		//输出访问信息
		System.out.println(c);
		//得到此顶点第一个邻接点的下标
		first = locateVertex(firstAdjVertex(c))-1;
		//若此下标不为-1
		while(first != -1) {
			System.out.println(first);
			//递归调用访问该顶点的第一个邻接点
			if(!this.flag[first]) depthFirstSearch(first);
			//访问该顶点的第二个邻接点
			first = locateVertex(nextAdjVertex(c, getVertex(first+1)))-1;
		}
	}
	
	
	//根据邻接矩阵变化的深度优先遍历
	public void depthFirstSearchMat(int i) {
		char c;
		int first = 0;
		//标志变为true, 代表已经访问过此顶点
		this.flag[i] = true;
		c = getVertex(i+1);
		//输出访问信息
		System.out.println(c);
		for(int j = 0; j < this.vernum; j++) {
			if(this.arcs[i][j].flag == true && !flag[i])
				depthFirstSearchMat(j);
		}
	}
	
	
	//打印此图的顶点数组
	public void printAllVertex() {
		System.out.println("该图的顶点数组如下:");
		for(int i = 0; i < this.vernum; i++) {
			System.out.print((i+1) + " ");
		}
		System.out.println();
		for(int i = 0; i < this.vernum; i++) {
			System.out.print(this.vertex[i] + " ");
		}
		System.out.println();
	}
	
	
	//打印此图的关系矩阵
	public void printAllArcs() {
		System.out.println("图的类型:     DG: 有向图, UDG: 无向图 ,DN: 有向网, UDN:无向网");
		System.out.println("此图类型为: " + this.kind + "\n");
		System.out.println("顶点之间关系如下图:\n");
		for(int i = 0; i <= this.vernum; i++) System.out.print(i + "(" + getVertex(i) + ")" + "\t");
		System.out.println("\n");
		for(int i = 0; i < this.vernum; i++) {
			System.out.print((i+1) + "(" + getVertex(i+1) + ")" +"\t");
			for(int j = 0; j < this.vernum; j++) {
				System.out.print(this.arcs[i][j].flag + "\t");
			}
			System.out.println("\n");
		}
		//打印边的之间的权值
		if(this.kind == Kind.DN || this.kind == Kind.UDN) {
			System.out.println("顶点之间权值如下图:\n");
			for(int i = 0; i <= this.vernum; i++) System.out.print(i + "(" + getVertex(i) + ")" + "\t");
			System.out.println("\n");
			for(int i = 0; i < this.vernum; i++) {
				System.out.print((i+1) + "(" + getVertex(i+1) + ")" +"\t");
				for(int j = 0; j < this.vernum; j++) {
					if(this.arcs[i][j].weight == this.INFINITY) System.out.print("No" + "\t");
					else System.out.print(this.arcs[i][j].weight + "\t");
				}
				System.out.println("\n");
			}
		}
	}

}


//定义两个顶点之间的弧结点
class ArcNode{
	int start;		//起始点的坐标
	int end;		//终止点的坐标
	boolean flag;	//两点是否有关系
	int weight = AdjMatrix.INFINITY;	//权值初始化为 无穷大
}